const net = require('net');

const PORT = 1338
const IP = '127.0.0.1'

var client = new net.Socket();

client.connect(PORT, IP, function() {
	console.log('Connected');
});

client.on('data', function(data) {
	let recvd = data.toString('utf8')
	console.log(recvd);
	if (recvd.includes('Enter 4-digit')) {
		client.write('1234');
	}
});

client.on('close', function() {
	console.log('Connection closed');
});