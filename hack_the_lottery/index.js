var net = require('net');

const max_iterations = 20;
const interval = 20000
const HOST_PORT = 1337
const HOST_IP = '192.168.0.103'

var draw_time = 1;
var calc_draw = [ 1, 2, 3, 4, 5, 6 ];
var global_seed = 1234;

var client = new net.Socket();
client.connect(HOST_PORT, HOST_IP, function() {
    console.log('Connection open');
});

client.on('data', function(data) {
    var recvd = data.toString('utf8')
    if (recvd.includes('Choose six numbers between ')) {
        draw_time = (Math.floor(Date.now() / interval) + 1) * interval;
        while (draw_time - Date.now() < interval / 2) draw_time += interval;

        calc_draw = rng_6(draw_time, global_seed)
        client.write(calc_draw.join(' '));
    }
    else if (recvd.includes('Draw result:  ')) {

        var actual_draw_values = recvd.split('Draw result:  ')[1].trim();
        console.log('Result    - %s', actual_draw_values);
        console.log('Draw Time - %d', draw_time);

        var actual_draw = actual_draw_values.split('-');
        // Will populate the global_seed if it finds it
        calculate_global_seed(draw_time, actual_draw)
    }
    
    if (recvd.includes('You win!')) {
        console.log('I won!!');
        console.log(recvd);
    }
});

function calculate_global_seed(draw_time, actual_draw) {
    
    var seed = 0;
    for (var seed = 0; seed < interval; seed++) {
        
        test_draw = rng_6(draw_time, seed);
        
        if ((test_draw[0] == actual_draw[0]) &&
            (test_draw[1] == actual_draw[1]) &&
            (test_draw[2] == actual_draw[2]) &&
            (test_draw[3] == actual_draw[3]) &&
            (test_draw[4] == actual_draw[4]) &&
            (test_draw[5] == actual_draw[5])) {
            global_seed = seed
            console.log('Seed found = %d', global_seed)
            break
        }
    }

    if (seed >= interval) {
        console.log('Seed not found, using previous value - %s', global_seed)
    }
}

function rng_6(draw_time, input_seed) {
    // Add the global seed to the next draw time to get the draw seed
    let draw_seed = reverse(draw_time + input_seed);
    let rng_iv = draw_seed;
    
    // Draw unique numbers from the seeded RNG
    let draw = [];
    let counter = 0;
    while (draw.length < 6) {
        let a = (rng_iv % 99) + 1;
        rng_iv = Math.floor(rng_iv / a);
        rng_iv = rng_iv * (99 - a + 1);
        counter += 1;
        if (counter > max_iterations) {
            break;
        }
        if (draw.indexOf(a) < 0) draw.push(a);
    }
    return draw;
}

function reverse(i) {
    return Number(
        String(i)
            .split('')
            .reverse()
            .join('')
    );
}

client.on('close', function() {
    console.log('Connection closed');
});
